# signaldctl
*a command line tool for [signald](https://gitlab.com/signald/signald)*

# Install

If you have the [signald debian repo](https://signald.org/articles/install/debian/) installed:

```
sudo apt install signaldctl
```

otherwise, download a binary from the CI:
* [Linux amd64](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl?job=build%3Ax86)
* [Linux arm](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl-linux-arm?job=build%3Across-compile%3A%20%5Blinux%2C%20arm%5D)
* [Linux arm64](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl?job=build%3Aaarch64)
* [Mac amd64](https://gitlab.com/api/v4/projects/21018340/jobs/artifacts/main/raw/signaldctl-darwin-amd64?job=build%3Across-compile%3A%20%5Bdarwin%2C%20amd64%5D)

or build it yourself:

```
go get gitlab.com/monkeymantra/signald-go/cmd/signaldctl
```

# Use

[see in progress documentation site](https://signald.org/signaldctl/)