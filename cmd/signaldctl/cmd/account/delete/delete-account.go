// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package delete

import (
	"log"

	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	server bool

	DeleteAccountCmd = &cobra.Command{
		Use:   "delete [phone number]",
		Short: "delete an account from the local signald instance, and optionally from the server as well",
		PreRun: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				common.Must(cmd.Help())
				log.Fatal("must specify phone number")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			req := v1.DeleteAccountRequest{Account: args[0]}
			err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error from signald: ", err)
			}
			log.Println("account deleted")
		},
	}
)

func init() {
	DeleteAccountCmd.Flags().BoolVar(&server, "server", false, "delete the account from the server as well")
}
