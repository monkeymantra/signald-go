// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package list

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	v1 "gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	ListAccountCmd = &cobra.Command{
		Use:   "list",
		Short: "return a list of accounts",
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)

			req := v1.ListAccountsRequest{}
			accounts, err := req.Submit(common.Signald)

			if err != nil {
				log.Fatal(err)
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(accounts.Accounts)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(accounts.Accounts)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"Phone Number", "UUID", "Device ID"})

				for _, account := range accounts.Accounts {
					t.AppendRow(table.Row{account.Address.Number, account.Address.UUID, account.DeviceId})
				}

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)
