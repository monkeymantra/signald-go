// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package verify

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	v1 "gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	VerifyAccountCmd = &cobra.Command{
		Use:   "verify [phone number] [code]",
		Short: "verify an account and complete the registration process",
		PreRun: func(cmd *cobra.Command, args []string) {
			if len(args) != 2 {
				common.Must(cmd.Help())
				log.Fatal("must specify phone number and code")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			req := v1.VerifyRequest{
				Account: args[0],
				Code:    args[1],
			}
			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err)
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatDefault:
				fmt.Println("verification succeeded")
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)
