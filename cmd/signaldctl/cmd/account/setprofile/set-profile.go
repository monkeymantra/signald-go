// Copyright © 2021 The signald-go Contributors.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package setprofile

import (
	"log"

	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account string

	SetProfileCmd = &cobra.Command{
		Use:   "set-profile name",
		Short: "updates the profile data with a new name",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) != 1 {
				common.Must(cmd.Help())
				log.Fatal("must specify a name")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			req := v1.SetProfile{
				Account: account,
				Name:    args[0],
			}
			err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error from signald: ", err)
			}
			log.Println("profile set")
		},
	}
)

func init() {
	SetProfileCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
