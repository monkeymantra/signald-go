// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
package remoteconfig

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	v1 "gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account         string
	RemoteConfigCmd = &cobra.Command{
		Use:   "remote-config",
		Short: "return a list of accounts",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			req := &v1.RemoteConfigRequest{Account: account}
			remoteconfig, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err)
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(remoteconfig.Config)
				if err != nil {
					log.Fatal("error encoding response to stdout:", err)
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(remoteconfig.Config)
				if err != nil {
					log.Fatal("error encoding response to stdout:", err)
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"Key", "Value"})
				for _, config := range remoteconfig.Config {
					t.AppendRow(table.Row{config.Name, config.Value})
				}
				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	RemoteConfigCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
