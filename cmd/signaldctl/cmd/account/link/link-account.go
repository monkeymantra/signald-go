// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package link

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/mdp/qrterminal"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/signald"
	v1 "gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	testing    bool
	deviceName string

	LinkAccountCmd = &cobra.Command{
		Use:   "link",
		Short: "create a local account by linking to an existing Signal account",
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			req := v1.GenerateLinkingURIRequest{}
			if testing {
				req.Server = signald.StagingServerUUID
			}

			response, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err)
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(response.Uri)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(response.Uri)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"URI"})
				t.AppendRow(table.Row{response.Uri})
				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			case common.OutputFormatQR, common.OutputFormatDefault:
				qrterminal.Generate(response.Uri, qrterminal.M, os.Stdout)
			default:
				log.Fatal("unsupported output format")
			}

			finishReq := v1.FinishLinkRequest{
				DeviceName: deviceName,
				SessionId:  response.SessionId,
			}

			_, err = finishReq.Submit(common.Signald)

			if err != nil {
				log.Fatal(err)
			}
			log.Println("linking successful")
		},
	}
)

func init() {
	name := "signald"
	if hostname, err := os.Hostname(); err == nil {
		name = fmt.Sprintf("signald on %s", hostname)
	}
	LinkAccountCmd.Flags().BoolVarP(&testing, "testing", "t", false, "use the Signal testing server")
	LinkAccountCmd.Flags().StringVarP(&deviceName, "device-name", "n", name, "the name of this device. shown to other devices on the signal account")
}
