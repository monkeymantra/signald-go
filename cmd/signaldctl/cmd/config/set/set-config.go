// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package set

import (
	"log"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
)

var (
	SetConfigCmd = &cobra.Command{
		Use:         "set <key> <value>",
		Short:       "send a message",
		Annotations: map[string]string{common.AnnotationNoSocketConnection: "true"},
		PreRun: func(cmd *cobra.Command, args []string) {
			if len(args) != 2 {
				common.Must(cmd.Help())
				log.Fatal("unexpected number of arguments")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			switch strings.ToLower(args[0]) {
			case "defaultaccount":
				config.Config.DefaultAccount = args[1]
			case "socketpath":
				config.Config.SocketPath = args[1]
			default:
				log.Fatal("unknown key ", args[0])
			}
			err := config.Save()
			if err != nil {
				log.Fatal("Error saving config: ", err)
			}
		},
	}
)
