// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"log"
	"path"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
)

var docCmd = &cobra.Command{
	Use:         "doc",
	Aliases:     []string{"docs"},
	Hidden:      true,
	Annotations: map[string]string{common.AnnotationNoSocketConnection: "true"},
	Run: func(_ *cobra.Command, args []string) {
		var err error
		outpath := "./"
		if len(args) > 0 {
			outpath = args[0]
		}
		switch common.OutputFormat {
		case common.OutputFormatMan:
			err = doc.GenManTree(RootCmd, &doc.GenManHeader{Title: "signaldctl"}, outpath)
		case common.OutputFormatMarkdown:
			err = doc.GenMarkdownTreeCustom(RootCmd, outpath, filePrepender, linkHandler)
		case common.OutputFormatYAML:
			err = doc.GenYamlTree(RootCmd, outpath)
		}
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(docCmd)
}

const fmTemplate = `---
title: "%s"
---
`

func filePrepender(filename string) string {
	name := filepath.Base(filename)
	base := strings.TrimSuffix(name, path.Ext(name))
	return fmt.Sprintf(fmTemplate, strings.Replace(base, "_", " ", -1))
}

func linkHandler(name string) string {
	base := strings.TrimSuffix(name, path.Ext(name))
	return "/signaldctl/reference/" + strings.ToLower(base) + "/"
}
