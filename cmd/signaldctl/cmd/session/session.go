// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package session

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account string

	ResetCmd = &cobra.Command{
		Use:   "reset <phone number | UUID>",
		Short: "reset secure session with another Signal user",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) == 0 {
				common.Must(cmd.Help())
				log.Fatal("must specify a phone number or UUID to reset session with")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)

			req := v1.ResetSessionRequest{Account: account}

			if strings.HasPrefix(args[0], "+") {
				req.Address = &v1.JsonAddress{Number: args[0]}
			} else {
				req.Address = &v1.JsonAddress{UUID: args[0]}
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error sending request to signald: ", err)
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"Number", "UUID", "Duration", "Send Error"})
				for _, result := range resp.Results {
					if result.Success != nil {
						t.AppendRow(table.Row{
							result.Address.Number,
							result.Address.UUID,
							fmt.Sprintf("%dms", result.Success.Duration),
							"",
						})
					} else {
						var sendError string
						if result.IdentityFailure != "" {
							sendError = fmt.Sprintf("identity failure: %s\n", result.IdentityFailure)
						}
						if result.NetworkFailure {
							sendError = "network failure"
						}
						if result.UnregisteredFailure {
							sendError = "user not registered"
						}
						t.AppendRow(table.Row{result.Address.Number, result.Address.UUID, "", sendError})
					}
				}

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	ResetCmd.Flags().StringVarP(&account, "account", "a", "", "local account to use")
}
