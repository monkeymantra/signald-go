// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package react

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account   string
	author    v1.JsonAddress
	timestamp int64
	emoji     string
	group     string
	remove    bool

	ReactMessageCmd = &cobra.Command{
		Use:   "react <author> <timestamp> <emoji>",
		Short: "react to a message",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) != 3 {
				common.Must(cmd.Help())
			}
			var err error
			author, err = common.StringToAddress(args[0])
			if err != nil {
				log.Fatal(err)
			}
			timestamp, err = strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				log.Fatal("Unable to parse timestamp", args[1], ":", err.Error())
			}
			emoji = args[2]
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)

			req := v1.ReactRequest{
				Username: account,
				Reaction: &v1.JsonReaction{
					Emoji:               emoji,
					Remove:              remove,
					TargetAuthor:        &author,
					TargetSentTimestamp: timestamp,
				},
			}

			if group == "" {
				req.RecipientAddress = &author
			} else {
				req.RecipientGroupID = group
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error sending request to signald: ", err)
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"Number", "UUID", "Duration", "Send Error"})
				for _, result := range resp.Results {
					if result.Success != nil {
						t.AppendRow(table.Row{
							result.Address.Number,
							result.Address.UUID,
							fmt.Sprintf("%dms", result.Success.Duration),
							"",
						})
					} else {
						var sendError string
						if result.IdentityFailure != "" {
							sendError = fmt.Sprintf("identity failure: %s\n", result.IdentityFailure)
						}
						if result.NetworkFailure {
							sendError = "network failure"
						}
						if result.UnregisteredFailure {
							sendError = "not on"
						}
						t.AppendRow(table.Row{result.Address.Number, result.Address.UUID, "", sendError})
					}
				}

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	ReactMessageCmd.Flags().StringVarP(&account, "account", "a", "", "local account to use")
	ReactMessageCmd.Flags().StringVarP(&group, "group", "g", "", "the group ID of the group the original message was sent to, if it was a group message")
	ReactMessageCmd.Flags().BoolVarP(&remove, "remove", "r", false, "remove a reaction that was previously set")
}
