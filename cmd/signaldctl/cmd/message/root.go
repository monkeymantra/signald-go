// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package message

import (
	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/message/react"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/message/read"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/message/send"
)

var MessageCmd = &cobra.Command{
	Use:     "message",
	Aliases: []string{"msg", "messages"},
}

func init() {
	MessageCmd.AddCommand(send.SendMessageCmd)
	MessageCmd.AddCommand(react.ReactMessageCmd)
	MessageCmd.AddCommand(read.ReadMessageCmd)
}
