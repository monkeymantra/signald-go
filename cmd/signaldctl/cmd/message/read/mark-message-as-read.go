// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package read

import (
	"log"
	"strconv"

	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account string

	ReadMessageCmd = &cobra.Command{
		Use:   "mark-read <author> <timestamp> [<timestamp> [<timestamp>...]]",
		Short: "mark a message as read",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) < 2 {
				common.Must(cmd.Help())
				log.Fatal("must specify author and timestamp")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)

			timestamps := []int64{}
			for _, arg := range args[1:] {
				ts, err := strconv.ParseInt(arg, 10, 64)
				if err != nil {
					log.Fatal("unable to parse timestamp: ", arg)
				}
				timestamps = append(timestamps, ts)
			}

			req := v1.MarkReadRequest{
				Account:    account,
				Timestamps: timestamps,
				To:         &v1.JsonAddress{Number: args[0]},
			}

			err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error sending request to signald: ", err)
			}
		},
	}
)

func init() {
	ReadMessageCmd.Flags().StringVarP(&account, "account", "a", "", "local account to use")
}
