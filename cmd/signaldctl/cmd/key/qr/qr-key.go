// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package qr

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/mdp/qrterminal"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account      string
	address      v1.JsonAddress
	safetyNumber string
	QRKeyCmd     = &cobra.Command{
		Use:   "qr <phone number or UUID> [<safety number>]",
		Short: "show the QR code of another user's safety number that can be scanned. Uses the newest safety number by default, but an older safety number may be optionally requested",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) == 0 {
				common.Must(cmd.Help())
				log.Fatal("please specify an address")
			}
			var err error
			address, err = common.StringToAddress(args[0])
			if err != nil {
				log.Fatal(err)
			}
			if len(args) > 1 {
				safetyNumber = args[1]
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			req := v1.GetIdentitiesRequest{Account: account, Address: &address}
			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err, "error communicating with signald")
			}

			var identity *v1.IdentityKey
			for _, i := range resp.Identities {
				if safetyNumber == "" {
					if identity == nil || i.Added > identity.Added {
						identity = i
					}
				} else if i.SafetyNumber == safetyNumber {
					identity = i
				}
			}
			if identity == nil {
				log.Fatal("no matching identity found")
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(identity.QrCodeData)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(identity.QrCodeData)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatQR, common.OutputFormatDefault:
				qrDataBytes, err := base64.StdEncoding.DecodeString(identity.QrCodeData)
				if err != nil {
					log.Fatal("Error decoding qr code data: ", err)
				}
				qrterminal.Generate(string(qrDataBytes), qrterminal.L, os.Stdout)
				fmt.Println("\nSafety Number: ")
				for i, n := range identity.SafetyNumber {
					fmt.Printf("%c", n)
					if i%30 == 29 {
						fmt.Println()
					} else if i%5 == 4 {
						fmt.Print(" ")
					}
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	QRKeyCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
