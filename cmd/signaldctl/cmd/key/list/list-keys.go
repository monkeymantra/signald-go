// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package list

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account     string
	address     *v1.JsonAddress
	ListKeysCmd = &cobra.Command{
		Use:   "list [<phone number or UUID>]",
		Short: "list all known keys for another account",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) >= 1 {
				a, err := common.StringToAddress(args[0])
				if err != nil {
					log.Fatal(err)
				}
				address = &a
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			if address == nil {
				getAll()
			} else {
				getOne()
			}
		},
	}
)

func init() {
	ListKeysCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}

func getOne() {
	req := v1.GetIdentitiesRequest{Account: account, Address: address}
	resp, err := req.Submit(common.Signald)
	if err != nil {
		log.Fatal(err)
	}

	switch common.OutputFormat {
	case common.OutputFormatJSON:
		err := json.NewEncoder(os.Stdout).Encode(resp)
		if err != nil {
			log.Fatal("error encoding response to stdout:", err)
		}
	case common.OutputFormatYAML:
		err := yaml.NewEncoder(os.Stdout).Encode(resp)
		if err != nil {
			log.Fatal("error encoding response to stdout:", err)
		}
	case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
		t := table.NewWriter()
		t.SetOutputMirror(os.Stdout)
		t.AppendHeader(table.Row{"Safety Number", "Trust Level"})
		for _, identity := range resp.Identities {
			t.AppendRow(table.Row{identity.SafetyNumber, identity.TrustLevel})
		}
		if common.OutputFormat == common.OutputFormatCSV {
			t.RenderCSV()
		} else {
			common.StylizeTable(t)
			t.Render()
		}
	default:
		log.Fatal("Unsupported output format")
	}
}

func getAll() {
	req := v1.GetAllIdentities{Account: account}
	resp, err := req.Submit(common.Signald)
	if err != nil {
		log.Fatal(err)
	}

	switch common.OutputFormat {
	case common.OutputFormatJSON:
		err := json.NewEncoder(os.Stdout).Encode(resp)
		if err != nil {
			log.Fatal("error encoding response to stdout:", err)
		}
	case common.OutputFormatYAML:
		err := yaml.NewEncoder(os.Stdout).Encode(resp)
		if err != nil {
			log.Fatal("error encoding response to stdout:", err)
		}
	case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
		t := table.NewWriter()
		t.SetOutputMirror(os.Stdout)
		t.AppendHeader(table.Row{"Number", "UUID", "Safety Number", "Trust Level"})
		for _, keys := range resp.IdentityKeys {
			for _, identity := range keys.Identities {
				t.AppendRow(table.Row{keys.Address.Number, keys.Address.UUID, identity.SafetyNumber, identity.TrustLevel})
			}
		}
		if common.OutputFormat == common.OutputFormatCSV {
			t.RenderCSV()
		} else {
			common.StylizeTable(t)
			t.Render()
		}
	default:
		log.Fatal("Unsupported output format")
	}
}
