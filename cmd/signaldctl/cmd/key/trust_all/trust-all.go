// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package trust_all

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account     string
	addresses   []v1.JsonAddress
	TrustAllCmd = &cobra.Command{
		Use:   "trust-all [<phone number or UUID>]",
		Short: "mark all keys as trusted, optionally limiting by phone number or UUID",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			for _, address := range args {
				address, err := common.StringToAddress(address)
				if err != nil {
					log.Fatal(err)
				}
				addresses = append(addresses, address)
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			changed := []v1.IdentityKeyList{}
			if len(addresses) > 0 {
				for _, address := range addresses {
					identitiesReq := v1.GetIdentitiesRequest{Account: account, Address: &address}
					identities, err := identitiesReq.Submit(common.Signald)
					if err != nil {
						log.Fatal(err)
					}

					changedIdentities := []*v1.IdentityKey{}

					for _, identityKey := range identities.Identities {
						if identityKey.TrustLevel == "UNTRUSTED" {
							trustReq := v1.TrustRequest{
								Account:      account,
								Address:      &address,
								SafetyNumber: identityKey.SafetyNumber,
								TrustLevel:   "TRUSTED_UNVERIFIED",
							}
							err = trustReq.Submit(common.Signald)
							if err != nil {
								log.Fatal(err)
							}
							changedIdentities = append(changedIdentities, identityKey)
						}
					}
					if len(changedIdentities) > 0 {
						changed = append(changed, v1.IdentityKeyList{
							Address:    &address,
							Identities: changedIdentities,
						})
					}
				}
			} else {
				identitiesReq := v1.GetAllIdentities{Account: account}
				resp, err := identitiesReq.Submit(common.Signald)
				if err != nil {
					log.Fatal(err)
				}
				for _, user := range resp.IdentityKeys {
					changedIdentities := []*v1.IdentityKey{}
					for _, identityKey := range user.Identities {
						if identityKey.TrustLevel == "UNTRUSTED" {
							trustReq := v1.TrustRequest{
								Account:      account,
								Address:      user.Address,
								SafetyNumber: identityKey.SafetyNumber,
								TrustLevel:   "TRUSTED_UNVERIFIED",
							}
							err = trustReq.Submit(common.Signald)
							if err != nil {
								log.Fatal(err)
							}
							changedIdentities = append(changedIdentities, identityKey)
						}
					}
					if len(changedIdentities) > 0 {
						changed = append(changed, v1.IdentityKeyList{
							Address:    user.Address,
							Identities: changedIdentities,
						})
					}
				}
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(changed)
				if err != nil {
					log.Fatal("error encoding response to stdout:", err)
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(changed)
				if err != nil {
					log.Fatal("error encoding response to stdout:", err)
				}
			case common.OutputFormatCSV, common.OutputFormatTable:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"Number", "UUID", "Safety Number"})
				for _, user := range changed {
					for _, identity := range user.Identities {
						t.AppendRow(table.Row{user.Address.Number, user.Address.UUID, identity.SafetyNumber})
					}
				}
				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			case common.OutputFormatQuiet, common.OutputFormatDefault:
				return
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	TrustAllCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
