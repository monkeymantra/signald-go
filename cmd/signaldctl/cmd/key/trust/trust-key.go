// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package trust

import (
	"log"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account      string
	address      v1.JsonAddress
	safetyNumber string
	level        string
	TrustKeyCmd  = &cobra.Command{
		Use:   "trust <phone number or UUID> <safety number> [level]",
		Short: "set the trust level of another user's safety number (",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) < 2 {
				common.Must(cmd.Help())
				log.Fatal("please specify an address and a safety number")
			}
			var err error
			address, err = common.StringToAddress(args[0])
			if err != nil {
				log.Fatal(err)
			}
			safetyNumber = args[1]
			if len(args) > 2 {
				level = strings.ToUpper(args[2])
			} else {
				level = "TRUSTED_VERIFIED"
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			req := v1.TrustRequest{
				Account:      account,
				Address:      &address,
				SafetyNumber: safetyNumber,
				TrustLevel:   level,
			}
			err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error from signald: ", err)
			}
			log.Println("success")
		},
	}
)

func init() {
	TrustKeyCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
