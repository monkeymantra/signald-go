// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/account"
	configcmd "gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/config"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/device"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/key"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/message"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/session"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald"
)

var socketPath string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:               "signaldctl",
	Short:             "Interact with a running signald instance",
	Long:              `signaldctl is a command line tool to interact with signald.`,
	DisableAutoGenTag: true,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if socketPath == "" {
			socketPath = config.Config.SocketPath
		}

		if _, ok := cmd.Annotations[common.AnnotationNoSocketConnection]; ok {
			return
		}

		common.Signald = &signald.Signald{SocketPath: socketPath}
		if err := common.Signald.Connect(); err != nil {
			log.Fatal(err)
		}
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	defaultConfigPath := fmt.Sprintf("%s/.config/signaldctl.yaml", os.Getenv("HOME"))
	defaultSocketPath := ""
	if os.Getenv("SIGNALDCTL_PUBLIC_DOC_MODE") == "on" {
		defaultConfigPath = "~/.config/signaldctl.yaml"
		defaultSocketPath = "/var/run/signald/signald.sock"
	}
	RootCmd.PersistentFlags().StringVar(&config.Path, "config", defaultConfigPath, "config file")
	RootCmd.PersistentFlags().StringVar(&socketPath, "socket", defaultSocketPath, "the path to the signald socket file")
	RootCmd.PersistentFlags().StringVarP(&common.OutputFormat, "output-format", "o", "default", "the output format. options are usually table, yaml and json, default is usually table. Some commands have other options.")
	RootCmd.AddCommand(account.AccountCmd)
	RootCmd.AddCommand(configcmd.ConfigCmd)
	RootCmd.AddCommand(device.DeviceCmd)
	RootCmd.AddCommand(group.GroupCmd)
	RootCmd.AddCommand(key.KeyCmd)
	RootCmd.AddCommand(message.MessageCmd)
	RootCmd.AddCommand(session.SessionCmd)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if err := config.Load(); err != nil {
		log.Fatal("error loading config file:", err)
	}
}
