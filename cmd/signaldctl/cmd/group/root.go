// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package group

import (
	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/accept"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/addmember"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/create"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/leave"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/list"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/removemember"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/show"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/update"
)

var GroupCmd = &cobra.Command{
	Use:     "group",
	Aliases: []string{"groups"},
}

func init() {
	GroupCmd.AddCommand(accept.AcceptGroupInvitationCmd)
	GroupCmd.AddCommand(addmember.AddGroupMembersCmd)
	GroupCmd.AddCommand(create.CreateGroupCmd)
	GroupCmd.AddCommand(leave.LeaveGroupCmd)
	GroupCmd.AddCommand(list.ListGroupCmd)
	GroupCmd.AddCommand(removemember.RemoveMemberCmd)
	GroupCmd.AddCommand(show.ShowGroupCmd)
	GroupCmd.AddCommand(update.UpdateGroupCmd)
}
