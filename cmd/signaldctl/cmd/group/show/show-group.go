// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package show

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account      string
	ShowGroupCmd = &cobra.Command{
		Use:   "show <group id> [<group id>]",
		Short: "show details about a group",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) == 0 {
				common.Must(cmd.Help())
				log.Fatal("must specify at least one group id")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			groups := []v1.JsonGroupV2Info{}
			for _, group := range args {
				req := v1.GetGroupRequest{Account: account, GroupID: group}
				resp, err := req.Submit(common.Signald)
				if err != nil {
					log.Fatal(err, "error communicating with signald")
				}
				groups = append(groups, resp)
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				common.Must(json.NewEncoder(os.Stdout).Encode(groups))
			case common.OutputFormatYAML:
				common.Must(yaml.NewEncoder(os.Stdout).Encode(groups))
			case common.OutputFormatDefault:
				for i, group := range groups {
					if i > 0 {
						fmt.Println()
					}
					leftColumnWidth := 12
					fmt.Println(text.Pad("Title", leftColumnWidth, ' '), group.Title)
					fmt.Println(text.Pad("ID", leftColumnWidth, ' '), text.Pad(group.ID, 10, ' '))
					fmt.Println(text.Pad("Revision", leftColumnWidth, ' '), group.Revision)
					if group.Timer > 0 {
						fmt.Println(text.Pad("Timer", leftColumnWidth, ' '), text.NewUnixTimeTransformer("", nil)(group.Timer))
					}
					if group.InviteLink != "" {
						fmt.Println(text.Pad("Invite Link", leftColumnWidth, ' '), group.InviteLink)
					}
					fmt.Println("Members")

					// membere164s := make(map[string]string)
					// for _, member := range group.Members {
					// 	if member.Number == "" {
					// 		continue
					// 	}
					// 	membere164s[member.UUID] = member.Number
					// }

					t := table.NewWriter()
					t.AppendHeader(table.Row{"Number", "UUID", "Name", "Role"})
					for _, member := range group.MemberDetail {
						req := v1.GetProfileRequest{
							Account: account,
							Address: &v1.JsonAddress{UUID: member.UUID},
							Async:   true,
						}
						e164 := ""
						profile, err := req.Submit(common.Signald)
						if err != nil {
							log.Println("error getting profile: ", err)
						} else {
							e164 = profile.Address.Number
						}
						t.AppendRow(table.Row{e164, member.UUID, profile.ProfileName, member.Role})
					}
					t.SetOutputMirror(os.Stdout)
					common.StylizeTable(t)
					t.Render()

					if len(group.PendingMembers) > 0 {
						fmt.Println("Pending Members")

						pendinge164s := make(map[string]string)
						for _, member := range group.PendingMembers {
							if member.Number == "" {
								continue
							}
							pendinge164s[member.UUID] = member.Number
						}

						t := table.NewWriter()
						t.AppendHeader(table.Row{"Number", "UUID", "Role"})
						for _, member := range group.PendingMemberDetail {
							t.AppendRow(table.Row{pendinge164s[member.UUID], member.UUID, member.Role})
						}
						t.SetOutputMirror(os.Stdout)
						common.StylizeTable(t)
						t.Render()
					}

					if len(group.RequestingMembers) > 0 {
						fmt.Println("Requesting Members")

						t := table.NewWriter()
						t.AppendHeader(table.Row{"Number", "UUID"})
						for _, member := range group.RequestingMembers {
							t.AppendRow(table.Row{member.Number, member.UUID})
						}
						t.SetOutputMirror(os.Stdout)
						common.StylizeTable(t)
						t.Render()
					}

					fmt.Println("Access Control")
					t = table.NewWriter()
					t.AppendHeader(table.Row{"Property", "Required Permission"})
					t.AppendRows([]table.Row{{"Attributes", group.AccessControl.Attributes}, {"Link", group.AccessControl.Link}, {"Members", group.AccessControl.Members}})
					t.SetOutputMirror(os.Stdout)
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	ShowGroupCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
