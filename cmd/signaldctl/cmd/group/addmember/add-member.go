// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package addmember

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	"gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account string
	group   string
	members []*v1.JsonAddress

	AddGroupMembersCmd = &cobra.Command{
		Use:     "add-member <group id> [<new member number or UUID> [<new member number or UUID>...]]",
		Aliases: []string{"add-members"},
		Short:   "add a member to a group",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) < 2 {
				common.Must(cmd.Help())
				log.Fatal("not enough arguments provided")
			}
			group = args[0]
			for _, member := range args[1:] {
				address, err := common.StringToAddress(member)
				if err != nil {
					log.Fatal(err)
				}
				members = append(members, &address)
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			req := v1.UpdateGroupRequest{
				Account:    account,
				GroupID:    group,
				AddMembers: members,
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err, "error communicating with signald")
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"ID", "Title", "Members"})
				if resp.V2 != nil {
					t.AppendRow(table.Row{resp.V2.ID, resp.V2.Title, len(resp.V2.Members)})
				}

				if resp.V1 != nil {
					t.AppendRow(table.Row{resp.V1.GroupId, resp.V1.Name, len(resp.V1.Members)})
				}

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	AddGroupMembersCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
