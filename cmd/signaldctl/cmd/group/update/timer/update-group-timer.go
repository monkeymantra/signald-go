// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package timer

import (
	"encoding/json"
	"log"
	"os"
	"time"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	v1 "gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account             string
	group               string
	expiration          int32
	UpdateGroupTimerCmd = &cobra.Command{
		Use:   "timer <group> <time>",
		Short: "change a group's timer",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) != 2 {
				common.Must(cmd.Help())
				log.Fatal("not enough arguments provided")
			}
			group = args[0]

			d, err := time.ParseDuration(args[1])
			if err != nil {
				log.Fatal("unparsable time: see https://golang.org/pkg/time/#ParseDuration for format details")
			}
			expiration = int32(d.Seconds())
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)

			req := v1.UpdateGroupRequest{
				Account:     account,
				GroupID:     group,
				UpdateTimer: expiration,
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err, "error communicating with signald")
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"ID", "Title", "Members"})
				if resp.V2 != nil {
					t.AppendRow(table.Row{resp.V2.ID, resp.V2.Title, len(resp.V2.Members)})
				}

				if resp.V1 != nil {
					t.AppendRow(table.Row{resp.V1.GroupId, resp.V1.Name, len(resp.V1.Members)})
				}

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	UpdateGroupTimerCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
