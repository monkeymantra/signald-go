// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package update

import (
	"github.com/spf13/cobra"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/update/accesscontrol"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/update/avatar"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/update/role"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/update/timer"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/cmd/group/update/title"
)

var (
	UpdateGroupCmd = &cobra.Command{Use: "update"}
)

func init() {
	UpdateGroupCmd.AddCommand(accesscontrol.UpdateGroupAccessControlCmd)
	UpdateGroupCmd.AddCommand(avatar.UpdateGroupAvatarCmd)
	UpdateGroupCmd.AddCommand(role.UpdateGroupRoleCmd)
	UpdateGroupCmd.AddCommand(timer.UpdateGroupTimerCmd)
	UpdateGroupCmd.AddCommand(title.UpdateGroupTitleCmd)
}
