// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package accesscontrol

import (
	"encoding/json"
	"log"
	"os"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common"
	"gitlab.com/monkeymantra/signald-go/cmd/signaldctl/config"
	v1 "gitlab.com/monkeymantra/signald-go/signald/client-protocol/v1"
)

var (
	account                     string
	group                       string
	accesscontrol               v1.GroupAccessControl
	UpdateGroupAccessControlCmd = &cobra.Command{
		Use:     "access-control <group> <attributes|link|members> <any|admin|default|member|unknown|unsatisfiable>",
		Aliases: []string{"access-controls"},
		Short:   "change a group's access control",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) != 3 {
				common.Must(cmd.Help())
				log.Fatal("not enough arguments provided")
			}
			group = args[0]
			role := strings.ToUpper(args[2])
			if role == "ADMIN" {
				role = "ADMINISTRATOR"
			}
			switch args[1] {
			case "attributes":
				accesscontrol = v1.GroupAccessControl{Attributes: role}
			case "link":
				accesscontrol = v1.GroupAccessControl{Link: role}
			case "members":
				accesscontrol = v1.GroupAccessControl{Members: role}
			default:
				common.Must(cmd.Help())
				log.Fatal("please specify attribute, link or member")
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)

			req := v1.UpdateGroupRequest{
				Account:             account,
				GroupID:             group,
				UpdateAccessControl: &accesscontrol,
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err, "error communicating with signald")
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.AppendHeader(table.Row{"Property", "Required Permission"})
				t.AppendRows([]table.Row{{"Attributes", resp.V2.AccessControl.Attributes}, {"Link", resp.V2.AccessControl.Link}, {"Members", resp.V2.AccessControl.Members}})
				t.SetOutputMirror(os.Stdout)

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	UpdateGroupAccessControlCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
