module gitlab.com/monkeymantra/signald-go

go 1.14

require (
	github.com/google/uuid v1.3.0
	github.com/jedib0t/go-pretty/v6 v6.2.4
	github.com/mdp/qrterminal v1.0.1
	github.com/spf13/cobra v1.2.1
	gitlab.com/monkeymantra/signald-go v0.0.0-20210903204213-00fb1e745d83
	gopkg.in/yaml.v2 v2.4.0
)
