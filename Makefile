signaldctl: signald/client-protocol cmd/signaldctl
	go build -ldflags '-X gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common.Version=$(./version.sh) -X gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common.Branch=$(shell git rev-parse --abbrev-ref HEAD) -X gitlab.com/monkeymantra/signald-go/cmd/signaldctl/common.Commit=$(shell git rev-parse HEAD)' -o signaldctl ./cmd/signaldctl

protocol.json:
	signald --dump-protocol | jq . > protocol.json

signald/client-protocol: protocol.json tools/generator/*
	go run ./tools/generator < protocol.json

clean:
	rm -rf protocol.json
	rm -rf signald/client-protocol/*
